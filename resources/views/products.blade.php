@extends('layout')

@section('content')
    @if(count($products) > 0 )
        <div class="product">
            <div class="product-body">
                <table class="table table-stripped task-table">
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Image</th>
                        <th>Rating</th>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td class="table-text">
                                {{ $product['id'] }}
                            </td>
                            <td class="table-text">
                                {{ $product['name'] }}
                            </td>
                            <td class="table-text">
                                {{ $product['price'] }}
                            </td>
                            <td class="table-text">
                                {{ $product['img'] }}
                            </td>
                            <td>
                                {{ $product['rating'] }}
                            </td>
                        </tr>
                    @endforeach
                    @if($pagination)
                        @if($products->links())
                            <tr>
                                <td class="table-text" colspan="7">
                                    {{ $products->links() }}
                                </td>
                            </tr>
                        @endif
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection