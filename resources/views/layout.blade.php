<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'BSA-2019-php-6') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('assets/js/vendor/jquery.3.4.0.min.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/bootstrap.4.3.1.min.js') }}"></script>

    <!-- Styles -->
    <link href="{{ asset('assets/css/vendor/bootstrap.4.3.1.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/vendor/jumbotron.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">
</head>
<body>
@include('navbar')

<main role="main">
    @yield('content')
</main>

@include('footer')
</body>
</html>