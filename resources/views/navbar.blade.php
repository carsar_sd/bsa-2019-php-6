<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ Request::is('products') ? 'active' : null }}">
                <a class="nav-link" href="{{ url('/products') }}">All</a>
            </li>
            <li class="nav-item {{ Request::is('products/popular') ? 'active' : null }}">
                <a class="nav-link" href="{{ url('/products/popular') }}">Popular</a>
            </li>
            <li class="nav-item {{ Request::is('products/cheap') ? 'active' : null }}">
                <a class="nav-link" href="{{ url('/products/cheap') }}">Cheap</a>
            </li>
        </ul>
    </div>
</nav>