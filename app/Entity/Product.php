<?php

declare(strict_types=1);

namespace App\Entity;

class Product
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var float
     */
    public $price;

    /**
     * @var string
     */
    public $image;

    /**
     * @var float
     */
    public $rating;

    /**
     * Create a new product instance.
     *
     * @param int $id
     * @param string $name
     * @param float $price
     * @param string $image
     * @param float $rating
     *
     * @return void
     */
    public function __construct(int $id, string $name, float $price, string $image, float $rating)
    {
        $this->id     = $id;
        $this->name   = $name;
        $this->price  = $price;
        $this->img    = $image;
        $this->rating = $rating;
    }

    /**
     * @return int
     */
    public function getId():int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return void
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName():string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice():float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return void
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getImageUrl():string
    {
        return $this->img;
    }

    /**
     * @param string $image
     * @return void
     */
    public function setImageUrl(string $image)
    {
        $this->img = $image;
    }

    /**
     * @return float
     */
    public function getRating():float
    {
        return $this->rating;
    }

    /**
     * @param float $rating
     * @return void
     */
    public function setRating(float $rating)
    {
        $this->rating = $rating;
    }
}
