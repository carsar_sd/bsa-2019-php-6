<?php

namespace App\Http\Controllers;

use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductController extends Controller
{
    const ITEMS_PER_PAGE = 5;

    /**
     * @var GetAllProductsAction
     */
    private $allAction;

    /**
     * @var GetMostPopularProductAction
     */
    private $popularAction;

    /**
     * @var GetCheapestProductsAction
     */
    private $cheapAction;

    /**
     * ProductController constructor.
     *
     * @param GetAllProductsAction        $allAction
     * @param GetMostPopularProductAction $popularAction
     * @param GetCheapestProductsAction   $cheapAction
     */
    public function __construct(
        GetAllProductsAction $allAction,
        GetMostPopularProductAction $popularAction,
        GetCheapestProductsAction $cheapAction
    ) {
        $this->allAction = $allAction;
        $this->popularAction = $popularAction;
        $this->cheapAction = $cheapAction;
    }

    public function all(Request $request)
    {
        $products = ProductArrayPresenter::presentCollection(
            $this->allAction->execute()->getProducts()
        );

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($products);
        $perPage = self::ITEMS_PER_PAGE;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems = new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());

        return view('products', [
            'products' => $paginatedItems,
            'pagination' => true,
        ]);
    }

    public function popular()
    {
        $products[] = ProductArrayPresenter::present(
            $this->popularAction->execute()->getProduct()
        );

        return view('products', [
            'products' => $products,
            'pagination' => false,
        ]);
    }

    public function cheap()
    {
        $products = ProductArrayPresenter::presentCollection(
            $this->cheapAction->execute()->getProducts()
        );

        return view('products', [
            'products' => $products,
            'pagination' => false,
        ]);
    }
}
