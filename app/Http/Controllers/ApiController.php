<?php

namespace App\Http\Controllers;

use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;

class ApiController extends Controller
{
    /**
     * @var GetAllProductsAction
     */
    private $allAction;

    /**
     * @var GetMostPopularProductAction
     */
    private $popularAction;

    /**
     * @var GetCheapestProductsAction
     */
    private $cheapAction;

    /**
     * ApiController constructor.
     *
     * @param GetAllProductsAction        $allAction
     * @param GetMostPopularProductAction $popularAction
     * @param GetCheapestProductsAction   $cheapAction
     */
    public function __construct(
        GetAllProductsAction $allAction,
        GetMostPopularProductAction $popularAction,
        GetCheapestProductsAction $cheapAction
    ) {
        $this->allAction = $allAction;
        $this->popularAction = $popularAction;
        $this->cheapAction = $cheapAction;
    }

    /**
     * @return array
     */
    public function all()
    {
        return ProductArrayPresenter::presentCollection(
            $this->allAction->execute()->getProducts()
        );
    }

    /**
     * @return array
     */
    public function popular()
    {
        return ProductArrayPresenter::present(
            $this->popularAction->execute()->getProduct()
        );
    }

    /**
     * @return array
     */
    public function cheap()
    {
        return ProductArrayPresenter::presentCollection(
            $this->cheapAction->execute()->getProducts()
        );
    }
}
