<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Product;

class ProductGenerator
{
    /**
     * @return Product[]
     */
    public static function generate(): array
    {
        $products = [];

        for ($i = 1; $i <= 10; $i++) {
            $products[] = new Product(
                $i,
                'product' . $i,
                self::randomFloat(10, 100),
                'image_url' . $i,
                self::randomFloat(1, 10)
            );
        }

        return $products;
    }

    /**
     * @param int $min
     * @param int $max
     * @return float
     */
    private static function randomFloat(int $min = 1, int $max = 10):float
    {
        return $min + mt_rand() / mt_getrandmax() * ($max - $min);
    }
}
