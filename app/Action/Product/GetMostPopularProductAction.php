<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductAction
{
    /**
     * @return GetMostPopularProductResponse
     */
    public function execute(): GetMostPopularProductResponse
    {
        $repository = app(ProductRepositoryInterface::class);

        return new GetMostPopularProductResponse($repository->findPopular());
    }
}
