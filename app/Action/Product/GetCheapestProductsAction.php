<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    /**
     * @return GetCheapestProductsResponse
     */
    public function execute(): GetCheapestProductsResponse
    {
        $repository = app(ProductRepositoryInterface::class);

        return new GetCheapestProductsResponse($repository->findCheapest());
    }
}
