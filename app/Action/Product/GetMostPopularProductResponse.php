<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;

class GetMostPopularProductResponse
{
    /**
     * @var Product
     */
    private $product;

    /**
     * GetMostPopularProductResponse constructor.
     *
     * @param Product $product
     */
    public function __construct($product)
    {
        $this->product = $product;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
