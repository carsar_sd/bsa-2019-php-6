<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;


class GetAllProductsAction
{
    /**
     * @return GetAllProductsResponse
     */
    public function execute(): GetAllProductsResponse
    {
        $repository = app(ProductRepositoryInterface::class);

        return new GetAllProductsResponse($repository->findAll());
    }
}
