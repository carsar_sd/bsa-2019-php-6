<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;

class GetCheapestProductsResponse
{
    /**
     * @var Product[]
     */
    private $products;

    /**
     * GetCheapestProductsResponse constructor.
     *
     * @param Product[] $products
     */
    public function __construct($products)
    {
        $this->products = $products;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }
}
