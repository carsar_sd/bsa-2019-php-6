<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;

class ProductRepository implements ProductRepositoryInterface
{
    /**
     * @var Product[]
     */
    private $products;

    /**
     * @param Product[] $products
     */
    public function __construct(array $products)
    {
        $this->products = $products;
    }

    /**
     * @return Product[]
     */
    public function findAll(): array
    {
        return collect($this->products)->all();
    }

    /**
     * @return Product
     */
    public function findPopular():Product
    {
        $sorted = collect($this->products)->sortByDesc('rating');

        return $sorted->first();
    }

    /**
     * @return Product[]
     */
    public function findCheapest(): array
    {
        $sorted = collect($this->products)->sortBy('price');

        return $sorted->values()->slice(0, 3)->toArray();
    }
}
