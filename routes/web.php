<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('products')
    ->group(
        function() {
            Route::get('/', [
                'as' => 'all',
                'uses' => 'ProductController@all']);
            Route::get('/popular', [
                'as' => 'popular',
                'uses' => 'ProductController@popular']);
            Route::get('/cheap', [
                'as' => 'cheap',
                'uses' => 'ProductController@cheap']);
        }
    );
